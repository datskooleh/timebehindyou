﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Hoster
{
    class Program
    {
        private static int ApiPort = 5000;
        private static string BaseUrl = String.Format("http://localhost:_PORT_/");

        static void Main(String[] args)
        {
            string apiUrl = BaseUrl.Replace("_PORT_", ApiPort.ToString());
            
            using (WebApp.Start<TimeBehindYou.API.Startup>(apiUrl))
            {
                Console.WriteLine("API started. Url: {0}\n", apiUrl);

                Console.WriteLine("Press any key to exit");

                Console.ReadLine();
            }
        }

    }
}
