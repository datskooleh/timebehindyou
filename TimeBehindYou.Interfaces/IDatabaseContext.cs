﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Interfaces
{
    public interface IDatabaseContext<TEntity> where TEntity : class, IEntity, new
    {
        void Add<TEntity>(TEntity);
    }
}
