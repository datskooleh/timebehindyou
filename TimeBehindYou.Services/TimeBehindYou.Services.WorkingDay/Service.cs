﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using TimeBehindYou.Common;
using TimeBehindYou.Services.Common;
using TimeBehindYou.Services.WorkingDay.Models;
using WorkingDay_E = TimeBehindYou.Data.Entities.WorkingDay;
using WorkingDay_V = TimeBehindYou.Services.WorkingDay.Models.WorkingDay;

namespace TimeBehindYou.Services.WorkingDay
{
    public class Service : BaseService<WorkingDay_E, WorkingDay_V>
    {
        public Service(IUnityContainer container)
            : base(container)
        {
        }

        public IEnumerable<WorkingDay_V> GetAllForUser(Int32 userId)
        {
            return null;
        }

        public Result<WorkingDay_V> Find(Int32 Id)
        {
            Result<WorkingDay_V> result = new Result<WorkingDay_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            return result;
        }

        public Result<WorkingDay_V> UpdateDay(Int32 userId, WorkingDay_V workingDay)
        {
            Result<WorkingDay_V> result = new Result<WorkingDay_V>()
            {
                Data = workingDay,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //get last day from database.
            //compare to current
            //if is older - create new. Else - update

            return result;
        }

        public Result<Boolean> RemoveDay(Int32 dayId)
        {
            Result<Boolean> result = new Result<Boolean>()
            {
                Data = true,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            return result;
        }
    }
}
