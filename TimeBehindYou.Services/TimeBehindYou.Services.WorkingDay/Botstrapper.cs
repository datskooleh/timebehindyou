﻿using AutoMapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeBehindYou.Services.Common;

using WorkingDay_E = TimeBehindYou.Data.Entities.WorkingDay;
using WorkingDay_V = TimeBehindYou.Services.WorkingDay.Models.WorkingDay;

namespace TimeBehindYou.Services.WorkingDay
{
    public class Botstrapper : IServiceBootstrapper
    {
        public void ConfigureUnity(IUnityContainer container)
        {
            var user = System.Security.Principal.WindowsIdentity.GetCurrent();//.GetNewCurrentUser();
            container.RegisterInstance(user);
            TimeBehindYou.DataAccess.Bootstrapper.RegisterWithContainer(container);
        }

        public void ConfigureMappings()
        {
            Mapper.CreateMap<WorkingDay_E, WorkingDay_V>();

            Mapper.CreateMap<WorkingDay_V, WorkingDay_E>();

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
