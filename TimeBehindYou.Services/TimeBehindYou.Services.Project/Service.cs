﻿using Microsoft.Practices.Unity;
using System;
using System.Linq;
using System.Collections.Generic;
using TimeBehindYou.Common;
using TimeBehindYou.Services.Common;
using TimeBehindYou.Services.Project.Models;
using Project_E = TimeBehindYou.Data.Entities.Project;
using Project_V = TimeBehindYou.Services.Project.Models.Project;

namespace TimeBehindYou.Services.Project
{
    public class Service : BaseService<Project_E, Project_V>
    {
        public Service(IUnityContainer container)
            : base(container)
        {
        }

        public IEnumerable<Project_V> GetAll(Int32 userId)
        {
            return null;
        }

        public Result<Project_V> Get(Int32 userId, Int32 id)
        {
            Result<Project_V> result = new Result<Project_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            var project = base.Get().SingleOrDefault(x => x.Id == id);

            result.Data = project;
            result.Status = project == null ? TimeBehindYou.Common.Enums.ProcessingResult.NoContent : TimeBehindYou.Common.Enums.ProcessingResult.Ok;

            return result;
        }

        public Result<Project_V> Add(Project_V project)
        {
            Result<Project_V> result = new Result<Project_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            project.Tasks = new List<Task>();
            base.Save(project);

            //validate if user exist
            //validate if he can add project
            //add received project to the user projects

            return result;
        }

        public Result<Project_V> Update(Project_V project)
        {
            Result<Project_V> result = new Result<Project_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            if (project.Id <= 0)
            {
                result.Status = TimeBehindYou.Common.Enums.ProcessingResult.NoContent;
                return result;
            }

            base.Save(project);

            //validate if user exist
            //find project
            //if not founded return 404 code
            //update project
            //if failed return 500 code
            //return 200 code

            return result;
        }

        public bool Remove(Project_V project)
        {
            //get user Id and Project by user Id ????
            //if project exist - remove it.
            //else return error

            return true;
        }
    }
}
