﻿using AutoMapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Services.Common;

using Project_E = TimeBehindYou.Data.Entities.Project;
using Project_V = TimeBehindYou.Services.Project.Models.Project;

namespace TimeBehindYou.Services.Project
{
    public class Botstrapper : IServiceBootstrapper
    {
        public void ConfigureUnity(IUnityContainer container)
        {
            var user = System.Security.Principal.WindowsIdentity.GetCurrent();//.GetNewCurrentUser();
            container.RegisterInstance(user);
            TimeBehindYou.DataAccess.Bootstrapper.RegisterWithContainer(container);
        }

        public void ConfigureMappings()
        {
            Mapper.CreateMap<Project_E, Project_V>();

            Mapper.CreateMap<Project_V, Project_E>();

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
