﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeBehindYou.Data.Interfaces;
using TimeBehindYou.Services.Common;

namespace TimeBehindYou.Services.Project.Models
{
    public class Project : IServiceEntity
    {
        public Int32 Id { get; set; }

        public Int32 UserId { get; set; }

        public List<Task> Tasks { get; set; }
    }
}
