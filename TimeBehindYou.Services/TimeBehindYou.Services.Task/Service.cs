﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using TimeBehindYou.Common;
using TimeBehindYou.Services.Common;
using TimeBehindYou.Services.Task.Models;
using Task_E = TimeBehindYou.Data.Entities.Task;
using Task_V = TimeBehindYou.Services.Task.Models.Task;

namespace TimeBehindYou.Services.Task
{
    public class Service : BaseService<Task_E, Task_V>
    {
        public Service(IUnityContainer container)
            : base(container)
        {
        }

        public IEnumerable<Task_V> Get(Int32 userId, Func<Task_V, Boolean> filter = null)
        {
            //validate if user exist
            //return 400 if not found
            //find project
            //return 500 if not founded
            //check if user has access to this project
            //return 404 if he has no access
            //get all tasks from project
            IEnumerable<Task_V> data;

            if (filter == null)
                data = base.Get();
            else
                data = base.Get(filter);


            return data;
        }

        public Result<Task_V> Get(Task_V task)
        {
            Result<Task_V> result = new Result<Task_V>()
            {
                Data = task,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //validate if user exist
            //return 400 if not found
            //check if task exist
            //get project for the task
            //return 404 if not founded
            //validate if user has access to project
            //return 400 if not
            //return task

            return result;
        }

        public Result<Task_V> Delete(Task_V task)
        {
            Result<Task_V> result = new Result<Task_V>()
            {
                Data = task,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //check if user exist
            //return 400 if not
            //check if task exist
            //return 404 if not
            //check if user has access to task project
            //return 400 if not
            //remove task

            return result;
        }

        public Result<Task_V> Add(Int32 userId, Task_V task)
        {
            Result<Task_V> result = new Result<Task_V>()
            {
                Data = task,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //check if user exist
            //return 400 if not
            //check if user has access to task project
            //return 400 if not
            //add task

            return result;
        }

        public Result<Task_V> Update(Int32 userId, Task_V task)
        {
            Result<Task_V> result = new Result<Task_V>()
            {
                Data = task,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //check if user exist
            //return 400 if not
            //check if task exist
            //return 404 if not
            //check if user has access to task project
            //return 400 if not
            //update task

            return result;
        }
    }
}
