﻿using AutoMapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Services.Common;

using Task_E = TimeBehindYou.Data.Entities.Task;
using Task_V = TimeBehindYou.Services.Task.Models.Task;

namespace TimeBehindYou.Services.Task
{
    public class Botstrapper : IServiceBootstrapper
    {
        public void ConfigureUnity(IUnityContainer container)
        {
            var user = System.Security.Principal.WindowsIdentity.GetCurrent();//.GetNewCurrentUser();
            container.RegisterInstance(user);
            TimeBehindYou.DataAccess.Bootstrapper.RegisterWithContainer(container);
        }

        public void ConfigureMappings()
        {
            Mapper.CreateMap<Task_E, Task_V>();

            Mapper.CreateMap<Task_V, Task_E>();

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
