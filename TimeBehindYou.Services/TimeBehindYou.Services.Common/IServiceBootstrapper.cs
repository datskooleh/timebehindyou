﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Services.Common
{
    public interface IServiceBootstrapper
    {
        void ConfigureUnity(IUnityContainer container);
        void ConfigureMappings();
    }
}
