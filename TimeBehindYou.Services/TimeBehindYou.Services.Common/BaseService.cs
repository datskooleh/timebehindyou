﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Services.Common
{
    public class BaseService<TEntity, TServiceEntity> : IService
        where TServiceEntity : class, IServiceEntity
        where TEntity : class, IEntity
    {
        protected IUnityContainer Container { get; set; }

        public BaseService(IUnityContainer container)
        {
            Container = container;
        }

        public IEnumerable<TServiceEntity> Get()
        {
            IEnumerable<TServiceEntity> result = null;

            using (var db = Container.Resolve<IDatabaseContext>())
            {
                result = db.Query<TEntity>().Project<TEntity>().To<TServiceEntity>();
            }

            return result;
        }

        public bool Delete(TServiceEntity data)
        {
            using (var db = Container.Resolve<IDatabaseContext>())
            {
                var entity = Mapper.Map<TServiceEntity, TEntity>(data);

                db.Delete<TEntity>(entity);
            }

            return true;
        }

        public TServiceEntity Save(TServiceEntity data)
        {
            using (var db = Container.Resolve<IDatabaseContext>())
            {
                if (data.Id <= 0)
                {
                    var entity = Mapper.Map<TServiceEntity, TEntity>(data);

                    db.Add(entity);

                    db.SaveChanges();

                    if (entity != null)
                        return Mapper.Map<TEntity, TServiceEntity>(entity);

                    return null;
                }
                else
                {
                    return null;
                }

            }
        }
    }
}
