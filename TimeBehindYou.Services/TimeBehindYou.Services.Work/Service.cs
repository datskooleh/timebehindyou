﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using TimeBehindYou.Common;
using TimeBehindYou.Services.Common;
using TimeBehindYou.Services.Work.Models;
using Work_E = TimeBehindYou.Data.Entities.Work;
using Work_V = TimeBehindYou.Services.Work.Models.Work;

namespace TimeBehindYou.Services.Work
{
    public class Service : BaseService<Work_E, Work_V>
    {
        public Service(IUnityContainer container)
            : base(container)
        {
        }

        public IEnumerable<Work_V> Get(Int32 userId, Func<Work_V, Boolean> filter = null)
        {
            //validate if user exist
            //return 400 if not found
            //find project
            //return 500 if not founded
            //check if user has access to this project
            //return 404 if he has no access
            //get all tasks from project
            IEnumerable<Work_V> data;

            if (filter == null)
                data = base.Get();
            else
                data = base.Get(filter);


            return data;
        }

        public Result<Work_V> Get(Int32 userId, Int32 id)
        {
            Result<Work_V> result = new Result<Work_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            return result;
        }

        public Result<Work_V> Add(Work_V work)
        {
            Result<Work_V> result = new Result<Work_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //check if day exist
            //validate fields
            //add to day

            return result;
        }

        public Result<Work_V> Update(Work_V work)
        {
            Result<Work_V> result = new Result<Work_V>()
            {
                Data = work,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            return result;
        }

        public Result<Work_V> RemoveFromWorkingDay(Int32 workingDayId)
        {
            Result<Work_V> result = new Result<Work_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            return result;
        }
    }
}
