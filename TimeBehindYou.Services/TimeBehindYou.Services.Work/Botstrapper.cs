﻿using AutoMapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeBehindYou.Services.Common;

using Work_E = TimeBehindYou.Data.Entities.Work;
using Work_V = TimeBehindYou.Services.Work.Models.Work;

namespace TimeBehindYou.Services.Work
{
    public class Botstrapper : IServiceBootstrapper
    {
        public void ConfigureUnity(IUnityContainer container)
        {
            var user = System.Security.Principal.WindowsIdentity.GetCurrent();//.GetNewCurrentUser();
            container.RegisterInstance(user);
            TimeBehindYou.DataAccess.Bootstrapper.RegisterWithContainer(container);
        }

        public void ConfigureMappings()
        {
            Mapper.CreateMap<Work_E, Work_V>();

            Mapper.CreateMap<Work_V, Work_E>();

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
