﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Services.Common;

namespace TimeBehindYou.Services.Account
{
    public class Bootstrapper : IServiceBootstrapper
    {
        public void ConfigureUnity(IUnityContainer container)
        {
            container.RegisterInstance("SystemContainer");
        }

        public void ConfigureMappings()
        {
            AutoMapper.Mapper.AssertConfigurationIsValid();
        }

    }
}
