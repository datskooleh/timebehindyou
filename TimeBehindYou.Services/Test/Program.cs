﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Services.Common;

namespace Test
{
    public class Program
    {
        private static string DllPath
        {
            get
            {
                const string relative = @"..\..\..\..\library";
                var path = Environment.CurrentDirectory;
                var goUpCount = relative.Split('\\').Count(x => x == "..");
                return String.Join("\\", path.Split('\\').Take(path.Count(x => x == '\\') - goUpCount + 1).Union(relative.Split('\\').SkipWhile(x => x == "..")));
            }
        }

        private static readonly IUnityContainer Container = new UnityContainer();

        static void Main(string[] args)
        {
            var bootstrappers =
                new ReadOnlyCollection<IServiceBootstrapper>(
                    Directory.GetFiles(DllPath, "TimeBehindYou.Services.*.dll")
                        .Select(Assembly.LoadFile)
                        .SelectMany(x => x.GetTypes())
                        .Where(x => x.GetInterfaces().Contains(typeof(IServiceBootstrapper)))
                        .Select(Activator.CreateInstance)
                        .Cast<IServiceBootstrapper>()
                        .ToList());

            SetupUnity(bootstrappers);
            SetupAutoMapper(bootstrappers);

            Console.WriteLine("--Done--");
            Console.ReadLine();
        }

        private static void SetupUnity(IEnumerable<IServiceBootstrapper> bootstrappers)
        {
            //foreach (var bootstrapper in bootstrappers)
            //    bootstrapper.ConfigureUnity(Container);
        }

        private static void SetupAutoMapper(IEnumerable<IServiceBootstrapper> bootstrappers)
        {
            try
            {
                foreach (var bootstrapper in bootstrappers)
                {
                    Console.WriteLine(bootstrapper.GetType());
                    bootstrapper.ConfigureMappings();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
