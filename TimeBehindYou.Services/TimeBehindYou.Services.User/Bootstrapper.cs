﻿using AutoMapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Services.Common;
using User_E = TimeBehindYou.Data.Entities.User;
using User_V = TimeBehindYou.Services.User.Models.User;

namespace TimeBehindYou.Services.User
{
    public class Bootstrapper : IServiceBootstrapper
    {
        public void ConfigureUnity(IUnityContainer container)
        {
            //container.RegisterInstance("TimeBehindYou_DEV");
            var user = System.Security.Principal.WindowsIdentity.GetCurrent();//.GetNewCurrentUser();
            container.RegisterInstance(user);
            TimeBehindYou.DataAccess.Bootstrapper.RegisterWithContainer(container);
        }

        public void ConfigureMappings()
        {
            Mapper.CreateMap<User_E, User_V>();

            Mapper.CreateMap<User_V, User_E>()
                .ForMember(dest => dest.Birthday, opt => opt.Ignore())
                .ForMember(dest => dest.Claims, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEndDateUtc, opt => opt.Ignore())
                .ForMember(dest => dest.Logins, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.Roles, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.UserName, opt => opt.Ignore());

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
