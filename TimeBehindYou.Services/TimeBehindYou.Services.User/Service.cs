﻿using Microsoft.Practices.Unity;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Data.Interfaces;
using TimeBehindYou.Services.Common;

using User_E = TimeBehindYou.Data.Entities.User;
using User_V = TimeBehindYou.Services.User.Models.User;
using TimeBehindYou.Common;

namespace TimeBehindYou.Services.User
{
    public class Service : BaseService<User_E, User_V>
    {
        public Service(IUnityContainer container)
            : base(container)
        {
        }

        public IEnumerable<User_V> Get(Int32 userId, Func<User_V, Boolean> filter = null)
        {
            //validate if user exist
            //return 400 if not found
            //find project
            //return 500 if not founded
            //check if user has access to this project
            //return 404 if he has no access
            //get all tasks from project
            IEnumerable<User_V> data;

            if (filter == null)
                data = base.Get();
            else
                data = base.Get(filter);


            return data;
        }

        public Result<User_V> Find(Int32 userId, Int32 Id)
        {
            Result<User_V> result = new Result<User_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            if(Id < 1)
                return null;

            User_V user = null;

            using (var db = Container.Resolve<IDatabaseContext>())
            {
                user = db.Query<User_E>(x => x.Id == Id).Project().To<User_V>().SingleOrDefault();
            }

            result.Data = user;

            return result;
        }

        public Result<User_V> AddNew(User_V user)
        {
            Result<User_V> result = new Result<User_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //validate if fields fill right

            return result;
        }

        public Result<User_V> UpdateExist(User_V user)
        {
            Result<User_V> result = new Result<User_V>()
            {
                Data = null,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //validate if all fields filled right

            return result;
        }

        public Result<Boolean> Deactivate(User_V user)
        {
            Result<Boolean> result = new Result<Boolean>()
            {
                Data = true,
                Status = TimeBehindYou.Common.Enums.ProcessingResult.Ok
            };

            //just make this user non-returnable.

            return result;
        }
    }
}
