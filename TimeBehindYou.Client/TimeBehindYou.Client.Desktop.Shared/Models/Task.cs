﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public sealed class Task : Trackable
    {
        private String description;
        public String Description
        {
            get { return description; }
            set { OnPropertyChanged("TaskDescription"); }
        }

        private Int32 projectId;
        public Int32 ProjectId
        {
            get { return projectId; }
            set { projectId = value; OnPropertyChanged("TaskProjectId"); }
        }

        private Project project;
        public Project Project
        {
            get { return project; }
            set { project = value; OnPropertyChanged("TaskProject"); }
        }
    }
}
