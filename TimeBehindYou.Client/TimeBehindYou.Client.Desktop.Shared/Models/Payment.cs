﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Client.Desktop.Common;
using TimeBehindYou.Client.Desktop.Common.Enums;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public sealed class Payment : BaseNotifier
    {
        private Int32 id;
        public Int32 Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("PaymentId"); }
        }

        private Int32 typeCode;
        public Int32 TypeCode
        {
            get { return typeCode; }
            set { typeCode = value; OnPropertyChanged("PaymentTypeCode"); }
        }

        public PaymentType Type
        {
            get
            {
                return (PaymentType)TypeCode;
            }
            set
            {
                TypeCode = (Int32)value;
                OnPropertyChanged("PaymentType");
            }
        }

        private Decimal value;
        public Decimal Value
        {
            get { return this.value; }
            set { this.value = value; OnPropertyChanged("PaymentValue"); }
        }

        private Int32 currencyId;
        public Int32 CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; OnPropertyChanged("PaymentCurrencyId"); }
        }

        private Currency currency;
        public Currency Currency
        {
            get { return currency; }
            set { currency = value; OnPropertyChanged("PaymentCurrency"); }
        }
    }
}
