﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public sealed class Project : Trackable
    {
        public Project()
            : base()
        {
            Tasks = new ObservableCollection<Task>();
        }

        private Int32 userId;
        public Int32 UserId
        {
            get { return userId; }
            set { userId = value; OnPropertyChanged("ProjectUserId"); }
        }

        private User user;
        public User User
        {
            get { return user; }
            set { user = value; OnPropertyChanged("ProjectUser"); }
        }

        public ObservableCollection<Task> Tasks { get; set; }
    }
}
