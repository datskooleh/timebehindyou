﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Client.Desktop.Common;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public sealed class Currency : BaseNotifier
    {
        private Int32 id;
        public Int32 Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("CurrencyId"); }
        }

        private String description;
        public String Description
        {
            get { return description; }
            set { description = value; OnPropertyChanged("CurrencyDescription"); }
        }

        private String abbreviation;
        public String Abbreviation
        {
            get { return abbreviation; }
            set { abbreviation = value; OnPropertyChanged("CurrencyAbbreviation"); }
        }
    }
}
