﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Client.Desktop.Common;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public class Work : BaseNotifier
    {
        private Int32 id;
        public Int32 Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("WorkId"); }
        }

        private Int32 workingDayId;
        public Int32 WorkingDayId
        {
            get { return workingDayId; }
            set { workingDayId = value; OnPropertyChanged("WorkWorkingDayId"); }
        }

        private WorkingDay workingDay;
        public WorkingDay WorkingDay
        {
            get { return workingDay; }
            set { workingDay = value; OnPropertyChanged("WorkWorkingDay"); }
        }

        private Int32 trackableId;
        public Int32 TrackableId
        {
            get { return trackableId; }
            set { trackableId = value; OnPropertyChanged("WorkTrackableId"); }
        }

        private Trackable trackable;
        public Trackable Trackable
        {
            get { return trackable; }
            set { trackable = value; OnPropertyChanged("WorkTrackable"); }
        }

        private DateTime start;
        public DateTime Start
        {
            get { return start; }
            set { start = value; OnPropertyChanged("WorkStart"); }
        }

        private DateTime end;
        public DateTime End
        {
            get { return end; }
            set { end = value; OnPropertyChanged("WorkEnd"); }
        }

        private Int32 time;
        public Int32 Time
        {
            get { return time; }
            set { time = value; OnPropertyChanged("WorkTime"); }
        }
    }
}
