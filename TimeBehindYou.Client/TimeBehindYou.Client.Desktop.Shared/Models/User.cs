﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Client.Desktop.Common;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public class User : BaseNotifier
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("UserId"); }
        }

        public User()
        {
            Projects = new ObservableCollection<Project>();
            WorkingDays = new ObservableCollection<WorkingDay>();
            Payments = new ObservableCollection<Payment>();
        }

        private String token;
        public String Token
        {
            get { return token; }
            set { token = value; OnPropertyChanged("UserToken"); }
        }

        private String firstName;
        public String FirstName
        {
            get { return firstName; }
            set { firstName = value; OnPropertyChanged("UserFirstName"); }
        }

        private String lastName;
        public String LastName
        {
            get { return lastName; }
            set { lastName = value; OnPropertyChanged("UserLastName"); }
        }

        private DateTime? birthday;
        public DateTime? Birthday
        {
            get { return birthday; }
            set { birthday = value; OnPropertyChanged("UserBirthday"); }
        }

        public ObservableCollection<Project> Projects { get; set; }

        public ObservableCollection<WorkingDay> WorkingDays { get; set; }

        public ObservableCollection<Payment> Payments { get; set; }
    }
}
