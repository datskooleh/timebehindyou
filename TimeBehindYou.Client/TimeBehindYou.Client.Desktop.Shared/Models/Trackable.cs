﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Client.Desktop.Common;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public abstract class Trackable : BaseNotifier
    {
        public Trackable()
            : base()
        {
            Works = new ObservableCollection<Work>();
        }

        private Int32 id;
        public Int32 Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("TrackableId"); }
        }

        private String name;
        public String Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("TrackableName"); }
        }

        private Boolean isCompleted;
        public Boolean IsCompleted
        {
            get { return isCompleted; }
            set { isCompleted = value; OnPropertyChanged("TrackableIsCompleted"); }
        }

        private DateTime? deadline;
        public DateTime? Deadline
        {
            get { return deadline; }
            set { deadline = value; OnPropertyChanged("TrackableDeadline"); }
        }

        private Boolean canTrackAfterDeadline;
        public Boolean CanTrackAfterDeadline
        {
            get { return canTrackAfterDeadline; }
            set { canTrackAfterDeadline = value; OnPropertyChanged("TrackableCanTrackAfterDeadline"); }
        }

        public ObservableCollection<Work> Works { get; set; }
    }
}
