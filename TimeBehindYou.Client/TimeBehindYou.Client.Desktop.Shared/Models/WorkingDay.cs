﻿using System;
using System.Collections.ObjectModel;
using TimeBehindYou.Client.Desktop.Common;

namespace TimeBehindYou.Client.Desktop.Shared.Models
{
    public class WorkingDay : BaseNotifier
    {
        public WorkingDay()
        {
            Works = new ObservableCollection<Work>();
        }

        private Int32 id;
        public Int32 Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("WorkingDayId"); }
        }

        private Int32 userId;
        public Int32 UserId
        {
            get { return userId; }
            set { userId = value; OnPropertyChanged("WorkingDayUserId"); }
        }

        private User user;
        public User User
        {
            get { return user; }
            set { user = value; OnPropertyChanged("WorkingDayUser"); }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; OnPropertyChanged("WorkingDayDate"); }
        }

        public ObservableCollection<Work> Works { get; set; }
    }
}
