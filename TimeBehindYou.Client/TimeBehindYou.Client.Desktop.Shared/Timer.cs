﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Shared
{
    public class Timer
    {
        #region Fields
        private static readonly Int32 defaultInterval = 500;
        private static Double elapsedTimeMillis = 0.0;

        private static System.Threading.Timer internalTimer;

        private CancellationTokenSource ct = new CancellationTokenSource();
        #endregion

        #region events
        public delegate void ElapsedTimeChanged(Int32 elapsedMillis);
        public event ElapsedTimeChanged OnElapsedTimeChanged;
        #endregion

        public Int32 Interval { get; private set; }

        public Int32 ElapsedTime
        {
            get { return Convert.ToInt32(elapsedTimeMillis); }
        }

        #region ctors
        public Timer()
            : this(defaultInterval)
        {
        }

        public Timer(Int32 interval)
        {
            Interval = interval < 0 ? Timeout.Infinite : interval;

            internalTimer = new System.Threading.Timer(IntervalElapsed, null, Timeout.Infinite, Interval);
        }
        #endregion

        public void Start()
        {
            Task.Factory.StartNew(() => internalTimer.Change(0, Interval), TaskCreationOptions.AttachedToParent & TaskCreationOptions.LongRunning);
        }

        public void Pause()
        {
            internalTimer.Change(Timeout.Infinite, Interval);
        }

        public void Reset()
        {
            Pause();
            elapsedTimeMillis = default(Double);
        }

        private void IntervalElapsed(Object state)
        {
            elapsedTimeMillis += Interval;

            if (OnElapsedTimeChanged != null)
                OnElapsedTimeChanged(ElapsedTime / 1000);

            //Make screenshots here (screenshot timer is set through project/worker properties in the web/mobile client)
            //save elapsed time in localdb
            //initiate request to the server every N hours/minutes/seconds

            if (internalTimer == null)
                internalTimer.Change(0, Interval);
        }
    }
}
