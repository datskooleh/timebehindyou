﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Shared.Requests
{
    public static class RequestType
    {
        public static string Get { get { return "GET"; } }

        public static string Post { get { return "POST"; } }

        public static string Put { get { return "PUT"; } }

        public static string Delete { get { return "DELETE"; } }
    }
}
