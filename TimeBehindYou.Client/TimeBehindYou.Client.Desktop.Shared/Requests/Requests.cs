﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Shared.Requests
{
    public static class Requests
    {
        private static String baseUrl = "http://localhost:5000/api/";

        private static readonly Int32 notIncludedCount = -1;

        public static async Task<T> GetAsync<T>(Int32 id, String controller, String action, Int32 count)
        {
            System.Diagnostics.Contracts.Contract.Requires(id >= 0, "id must be specified");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(controller), "Enter controller");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(action), "Enter action");
            System.Diagnostics.Contracts.Contract.Requires(count >= -1, "Count must be bigger than -1");

            using (var client = new HttpClient())
            {
                PrepareClient(client);

                Uri uri = ParseUri(id, controller, action, count);

                HttpResponseMessage response = await client.GetAsync(uri);

                return await response.Content.ReadAsAsync<T>();
            }
        }

        public static async Task<T> PostAsync<T>(Int32 id, String controller, String action, T data) where T : class
        {
            System.Diagnostics.Contracts.Contract.Requires(id >= 0, "id must be specified");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(controller), "Enter controller");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(action), "Enter action");
            System.Diagnostics.Contracts.Contract.Requires(data != null, "Data must be not null");

            using (var client = new HttpClient())
            {
                PrepareClient(client);

                Uri uri = ParseUri(id, controller, action, notIncludedCount);

                HttpResponseMessage response = await client.PostAsync<T>(uri, data, new System.Net.Http.Formatting.JsonMediaTypeFormatter());

                return await response.Content.ReadAsAsync<T>();
            }
        }

        public static async Task<Boolean> PutAsync<T>(Int32 id, String controller, String action, T data) where T : class
        {
            System.Diagnostics.Contracts.Contract.Requires(id >= 0, "id must be specified");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(controller), "Enter controller");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(action), "Enter action");
            System.Diagnostics.Contracts.Contract.Requires(data != null, "Data must be not null");

            using (var client = new HttpClient())
            {
                PrepareClient(client);

                Uri uri = ParseUri(id, controller, action, notIncludedCount);

                HttpResponseMessage response = await client.PutAsync<T>(uri, data, new System.Net.Http.Formatting.JsonMediaTypeFormatter());

                return await response.Content.ReadAsAsync<Boolean>();
            }
        }

        public static async Task<Boolean> DeleteAsync<T>(Int32 id, String controller, String action, Int32 itemId) where T : class
        {
            System.Diagnostics.Contracts.Contract.Requires(id >= 0, "id must be specified");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(controller), "Enter controller");
            System.Diagnostics.Contracts.Contract.Requires(!String.IsNullOrWhiteSpace(action), "Enter action");

            using (var client = new HttpClient())
            {
                PrepareClient(client);

                Uri uri = ParseUri(id, controller, action, notIncludedCount);

                HttpResponseMessage response = await client.DeleteAsync(uri + "/" + itemId);

                return await response.Content.ReadAsAsync<Boolean>();
            }
        }

        #region Private
        private static void PrepareClient(HttpClient client)
        {
            if (client == null)
                throw new ArgumentNullException();

            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static Uri ParseUri(Int32 id, String controller, String action, Int32 count)
        {
            var includeCount = count != -1;

            var url = baseUrl + controller + '/' + action + (includeCount ? count == 0 ? "/100" : "/" + count : "");

            return new Uri(url);
        }
        #endregion
    }
}
