﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Client.Desktop.Common;

namespace TimeBehindYou.Client.Desktop.Windows.ViewModels
{
    public class LoginViewModel : BaseNotifier
    {
        public String ErrorMessage { get; set; }

        public String Login { get; set; }

        public String Password { get; set; }
    }
}
