﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Windows.Input;
using TimeBehindYou.Client.Desktop.Common;
using TimeBehindYou.Client.Desktop.Shared;
using TimeBehindYou.Client.Desktop.Shared.Models;
using TimeBehindYou.Client.Desktop.Shared.Requests;
using TimeBehindYou.Client.Desktop.Windows.ViewModels.Commands;

namespace TimeBehindYou.Client.Desktop.Windows.ViewModels
{
    public class MainWindowViewModel : BaseNotifier
    {
        public ObservableCollection<Trackable> TrackedWorks { get; set; }
        public Project TrackingProject { get; set; }
        public Task TrackingTask { get; set; }

        public Boolean CanChangeTracking
        {
            get
            {
                return TrackingProject != null;
            }
        }

        private static string defaultTimeValue = "00:00:00";

        private bool isTracking;

        public Timer Timer { get; set; }
        
        #region ctors
        public MainWindowViewModel()
        {
            Timer = new Shared.Timer();
            Timer.OnElapsedTimeChanged += TimerIntervalElapsed;

            startStopCommand = new RelayCommand(action => StartStopExecution(), action => CanExecuteStartStop());
        }
        #endregion

        private string startStop = "Start";
        public string StartStop
        {
            get
            {
                return startStop;
            }
            set
            {
                startStop = value;
                OnPropertyChanged("StartStop");

                //var result = Requests.GetAsync("http://localhost:5000/api/projects/10/").ContinueWith(res => { res.Status == System.Threading.Tasks.TaskStatus.RanToCompletion); });
            }
        }

        private string totalTaskWork = defaultTimeValue;
        public string TotalTaskWork
        {
            get
            {
                return totalTaskWork;
            }
            set
            {
                totalTaskWork = value;
                OnPropertyChanged("TotalTaskWork");
            }
        }

        private string totalWorkToday = defaultTimeValue;
        public string TotalWorkToday
        {
            get
            {
                return totalWorkToday;
            }
            set
            {
                totalWorkToday = value;
                OnPropertyChanged("TotalWorkToday");
            }
        }

        private ICommand startStopCommand;
        public ICommand StartStopCommand { get { return startStopCommand; } }

        #region Command Helpers
        private bool CanExecuteStartStop()
        {
            //return TrackingProject != null;
            return true;
        }

        private void StartStopExecution()
        {
            isTracking = !isTracking;

            if (isTracking)
            {
                StartStop = "Stop";
                Timer.Start();
            }
            else
            {
                StartStop = "Start";
                Timer.Reset();
                TotalTaskWork = defaultTimeValue;
            }
        }

        private void TimerIntervalElapsed(int elapsedSec)
        {
            var previousTaskWorkTime = TimeSpan.Parse(TotalTaskWork).TotalSeconds;

            TotalTaskWork = TimeSpan.FromSeconds(elapsedSec).ToString("c");

            var totalWorkInSeconds = TimeSpan.Parse(TotalWorkToday).TotalSeconds;
            totalWorkInSeconds += (elapsedSec - previousTaskWorkTime);
            Debug.WriteLine("Elapsed: " + elapsedSec);
            Debug.WriteLine("Previous: " + previousTaskWorkTime);

            TotalWorkToday = TimeSpan.FromSeconds(totalWorkInSeconds).ToString("c");
            Debug.WriteLine(TotalTaskWork);
        }
        #endregion
    }
}
