﻿using SQLite;
using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Windows.DataAccess
{
    public class Database<T> where T : new()
    {
        SQLiteConnection db;

        public Database()
        {
            db = DbConnection.Connection;
        }

        public T Find(System.Linq.Expressions.Expression<Func<T, Boolean>> predicate)
        {
            return db.Find<T>(predicate);
        }
    }
}
