﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Client.Desktop.Windows.DataAccess
{
    public static class DbConnection
    {
        public static SQLiteConnection Connection
        {
            get
            {
                return new SQLiteConnection("tbydb.db");
            }
        }
    }
}
