﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TimeBehindYou.Client.Desktop.Common.Enums
{
    public enum PaymentType
    {
        [Display(Name = "Hourly")]
        Hourly = 1,
        [Display(Name = "Monthly")]
        Monthly = 2,
        [Display(Name = "Fixed")]
        Fixed = 4,
        [Display(Name = "Once")]
        Once = 8
    }
}
