﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;

namespace TimeBehindYou.Data.Identity
{
    public class UserManager<TUser> : UserManager<TUser, Int32> where TUser: IdentityUser<Int32, UserLogin, UserRole, UserClaim>
    {
        public UserManager(IUserStore<TUser, Int32> store)
            : base(store)
        {
        }

        public static Identity.UserManager<TUser> Create(IdentityFactoryOptions<Identity.UserManager<TUser>> options, IOwinContext context)
        {
            var manager = new UserManager<TUser>(new UserStore<TUser>(context.Get<IdentityDbContext<TUser, MainRole, Int32, UserLogin, UserRole, UserClaim>>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<TUser, Int32>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<TUser, Int32>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}
