﻿using Microsoft.AspNet.Identity.EntityFramework;
using TimeBehindYou.Data.Identity;

namespace TimeBehindYou.Data.Identity
{
    using System;

    public class UserStore<TUser> : UserStore<TUser, MainRole, Int32, UserLogin, UserRole, UserClaim> where TUser: IdentityUser<Int32, UserLogin, UserRole, UserClaim>
    {
        public UserStore(System.Data.Entity.DbContext context)
            : base(context)
        {
        }
    }
}
