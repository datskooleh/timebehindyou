﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.DataAccess.Contexts;

namespace TimeBehindYou.DataAccess
{
    public class MigrationsContextFactory : IDbContextFactory<DatabaseContext>
    {
        public DatabaseContext Create()
        {
            return new DatabaseContext(new PackageConnection(TimeBehindYou.Connection.DataConnection.GetDefaultDbConnection()));
        }
    }
}
