﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using TimeBehindYou.Data.Entities;
using TimeBehindYou.Data.Identity;
using TimeBehindYou.Data.Interfaces;
using TimeBehindYou.DataAccess.Helpers;

namespace TimeBehindYou.DataAccess.Contexts
{
    public class DatabaseContext : IdentityDbContext<User, MainRole, Int32, UserLogin, UserRole, UserClaim>, IDatabaseContext
    {
        public DatabaseContext(IConnection connection)
            : base(connection.GetConnection(), false)
        {
        }

        //public DatabaseContext(String connectionString)
        //    : base(connectionString)
        //{
        //}

        public DatabaseContext()
            : base("TBY")
        {
        }

        [Dependency]
        public IUnityContainer Container { get; set; }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }

        #region DbSets
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Trackable> Trackables { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Work> Works { get; set; }
        public DbSet<WorkingDay> WorkingDays { get; set; }
        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                                          .Where(type => !String.IsNullOrEmpty(type.Namespace))
                                          .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                                                         type.BaseType.GetGenericTypeDefinition() ==
                                                         typeof(EntityTypeConfiguration<>));
            foreach (var configurationInstance in typesToRegister.Select(Activator.CreateInstance))
                modelBuilder.Configurations.Add((dynamic)configurationInstance);
            base.OnModelCreating(modelBuilder);
        }

        #region transaction
        private DbContextTransaction _currentTransaction;
        private readonly IList<String> _transactionSaveResults = new List<String>();

        public void TransactionBegin()
        {
            if (_currentTransaction != null)
                TransactionRollback();
            _currentTransaction = Database.BeginTransaction();
        }

        public void TransactionCommit()
        {
            if (_transactionSaveResults.Any(x => x.Equals("Failed")))
            {
                TransactionRollback();
            }
            else
            {
                _currentTransaction.Commit();

                TransactionCleanup();
            }
        }

        public void TransactionRollback()
        {
            if (_currentTransaction != null)
                try
                {
                    _currentTransaction.Rollback();
                }
                finally { }
            TransactionCleanup();
        }

        public bool TransactionActive { get { return _currentTransaction != null; } }

        private void TransactionCleanup()
        {
            // http://stackoverflow.com/questions/22486489/entity-framework-6-transaction-rollback
            //if(_currentTransaction != null)
            //    _currentTransaction.Dispose();

            _currentTransaction = null;
            _transactionSaveResults.Clear();
        }
        #endregion

        ~DatabaseContext()
        {
            base.Dispose();
        }

        #region IDatabaseContext
        public void Add<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            if (entities == null || entities.Count() == 0)
                return;

            foreach (var entity in entities)
                Add(entity);
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null)
                return;

            AddInternal<TEntity>(entity);
        }

        public void Delete<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            if (entities == null || entities.Count() == 0)
                return;

            foreach (var entity in entities)
                Update(entity);
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null || entity.Id < 0)
                return;

            DeleteInternal<TEntity>(entity);
        }

        public void Update<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            if (entities == null || entities.Count() == 0)
                return;

            foreach (var entity in entities)
                Update(entity);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null || entity.Id < 0)
                return;

            UpdateInternal<TEntity>(entity);
        }

        public void Detach<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            Entry<TEntity>(entity).State = EntityState.Detached;
        }

        bool IDatabaseContext.SaveChanges()
        {
            ChangeTracker.DetectChanges();

            foreach (var entry in ChangeTracker.Entries<IEntity>().Where(x => x.State != EntityState.Unchanged))
                CheckIfModified(entry);

            bool saveFailed;
            Exception saveException = null;

            int retryCount = 0, returnvalue = 0;

            do
            {
                try
                {

                    returnvalue = base.SaveChanges();
                    saveFailed = false;
                }
                catch (DbEntityValidationException ex)
                {
                    var validationErrors = "";
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        validationErrors += "\tEntry:" + error.Entry.Entity.GetType().Name + " - " + (error.Entry.Entity as IEntity).Id + Environment.NewLine;
                        foreach (var dbValidationError in error.ValidationErrors)
                        {
                            validationErrors += "\t\t" + dbValidationError.PropertyName + ": " + dbValidationError.ErrorMessage + Environment.NewLine;
                        }
                        validationErrors += Environment.NewLine + Environment.NewLine;
                    }
                    saveException =
                        new Exception(
                            "SAVE EXCEPTION" + Environment.NewLine + validationErrors,
                            ex);
                    saveFailed = true;
                }
                catch (DbUpdateException ex)
                {
                    saveException =
                        new Exception(
                            "SAVE EXCEPTION" + Environment.NewLine + "Entries: " +
                            String.Join(", ",
                                        ex.Entries.Select(
                                            x => x.Entity.GetType().Name + ": " + (x.Entity as IEntity).Id)),
                            ex);
                    if (ex.Entries.Any())
                    {
                        foreach (var entry in ex.Entries.Where(entry => entry.State != EntityState.Added))
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    }

                    saveFailed = true;
                    if (!(ex.InnerException is OptimisticConcurrencyException)) continue;
                    var ose = (ex.InnerException as OptimisticConcurrencyException).StateEntries.First();
                    ((IObjectContextAdapter)this).ObjectContext.Refresh(refreshMode: RefreshMode.StoreWins, entity: ose.Entity);
                }
                catch (OptimisticConcurrencyException ex)
                {
                    saveException = ex;
                    saveFailed = true;

                    // update original values from the database
                    var entry = ex.StateEntries.Single();
                    ((IObjectContextAdapter)this).ObjectContext.Refresh(refreshMode: RefreshMode.StoreWins, entity: entry.Entity);
                }
            } while (saveFailed && (retryCount++) < 10);

            if (saveFailed)
            {
                //log here
                return false;
            }

            if (TransactionActive)
                _transactionSaveResults.Insert(0, "Success");

            return true;
        }
        #endregion

        #region IDataQuery
        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return Set<TEntity>();
        }

        public IQueryable Query<TEntity>(Type type) where TEntity : class, IEntity
        {
            return Set(type);
        }

        public IQueryable<TEntity> Query<TEntity>(Func<TEntity, bool> func) where TEntity : class, IEntity
        {
            return Set<TEntity>().Where(func).AsQueryable<TEntity>();
        }

        #endregion

        #region Internal methods
        private void AddInternal<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var set = Set<TEntity>();
            set.Add(entity);
        }

        private void UpdateInternal<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            TEntity current = GetCurrentEntity<TEntity>(entity);

            if (current != null && Entry<TEntity>(current).IsAdded<TEntity>())
            {
                Entry<TEntity>(current).CurrentValues.SetValues(entity);
                Entry<TEntity>(current).State = EntityState.Modified;
            }
        }

        private void DeleteInternal<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var current = GetCurrentEntity<TEntity>(entity);

            if (Entry<TEntity>(current).IsAdded())
                Detach<TEntity>(current);
            else
                Set<TEntity>().Remove(current);
        }

        private TEntity GetCurrentEntity<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var current = Set<TEntity>().FirstOrDefault(x => x.Id.Equals(entity.Id));

            return current;
        }

        private void CheckIfModified(DbEntityEntry<IEntity> entry)
        {
            //TODO: does this method work for reference properties? Sure?
            if (entry.State != EntityState.Modified) return;
            var orig = entry.OriginalValues;
            var curr = entry.CurrentValues;

            var changed = false;
            foreach (var propertyName in orig.PropertyNames)
            {
                var origValue = orig[propertyName];
                var curValue = curr[propertyName];
                var testsNotChanged = new[]
                    {
                        origValue == curValue,
                        (origValue ?? new object()).Equals(curValue ?? new object()),
                        (origValue is byte[] && ((byte[]) origValue).SequenceEqual((byte[]) curValue))
                    };
                if (testsNotChanged.Any(x => x)) continue;
                changed = true;
                break;
            }

            if (!changed)
                entry.State = EntityState.Unchanged;
        }
        #endregion
    }
}
