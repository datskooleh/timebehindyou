﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Data.Identity;

namespace TimeBehindYou.DataAccess.Contexts
{
    public class RoleStore : RoleStore<MainRole, Int32,  UserRole>
    {
        public RoleStore(System.Data.Entity.DbContext context) :
            base(context)
        {

        }
    }
}
