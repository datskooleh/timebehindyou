﻿using Microsoft.Practices.Unity;
using System.Data.Entity;
using TimeBehindYou.Data.Interfaces;
using TimeBehindYou.DataAccess.Contexts;
using TimeBehindYou.DataAccess.Migrations;

namespace TimeBehindYou.DataAccess
{
    public static class Bootstrapper
    {
        public static void RegisterWithContainer(IUnityContainer container)
        {
            TimeBehindYou.Connection.Bootstrapper.RegisterWithContainer(container);

            container.RegisterType<IDatabaseContext, DatabaseContext>();

#if DEBUG
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseContext, Configuration>());
#else
            Database.SetInitializer<DatabaseContext>(null);
#endif
        }
    }
}
