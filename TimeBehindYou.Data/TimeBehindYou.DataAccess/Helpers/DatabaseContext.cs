﻿using System.Data.Entity.Infrastructure;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.DataAccess.Helpers
{
    internal static class DatabaseContext
    {
        internal static bool IsAdded<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : class, IEntity
        {
            return entry.State == System.Data.Entity.EntityState.Added;
        }

        internal static bool IsDeleted<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : class, IEntity
        {
            return entry.State == System.Data.Entity.EntityState.Deleted;
        }

        internal static bool IsDetached<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : class, IEntity
        {
            return entry.State == System.Data.Entity.EntityState.Detached;
        }

        internal static bool IsModified<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : class, IEntity
        {
            return entry.State == System.Data.Entity.EntityState.Modified;
        }

        internal static bool IsUnchanged<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : class, IEntity
        {
            return entry.State == System.Data.Entity.EntityState.Unchanged;
        }

    }
}
