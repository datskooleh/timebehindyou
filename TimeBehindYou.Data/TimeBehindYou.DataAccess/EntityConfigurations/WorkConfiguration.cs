﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class WorkConfiguration : EntityTypeConfiguration<Work>
    {
        public WorkConfiguration()
        {
            ToTable("Works");

            HasKey<Int32>(work => work.Id);
            Property<Int32>(work => work.Id).IsRequired();

            Property<DateTime>(work => work.Start).IsRequired();
            Property<DateTime>(work => work.End).IsRequired();

            Property<Int32>(work => work.Time).IsRequired();
        }
    }
}
