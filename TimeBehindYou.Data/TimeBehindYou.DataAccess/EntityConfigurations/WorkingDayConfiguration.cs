﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class WorkingDayConfiguration : EntityTypeConfiguration<WorkingDay>
    {
        public WorkingDayConfiguration()
        {
            ToTable("WorkingDays");

            HasKey<Int32>(wd => wd.Id);
            Property<Int32>(wd => wd.Id).IsRequired();

            Property<DateTime>(wd => wd.Date).IsRequired();

            HasMany<Work>(wd => wd.Works)
                .WithRequired(work => work.WorkingDay)
                .HasForeignKey<Int32>(work => work.WorkingDayId)
                .WillCascadeOnDelete(true);
        }
    }
}
