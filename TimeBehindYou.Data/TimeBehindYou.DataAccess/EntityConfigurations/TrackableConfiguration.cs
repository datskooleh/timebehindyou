﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class TrackableConfiguration : EntityTypeConfiguration<Trackable>
    {
        public TrackableConfiguration()
        {
            ToTable("Trackables");

            HasKey<Int32>(task => task.Id);
            Property<Int32>(task => task.Id).IsRequired();

            Property(task => task.Name).IsRequired();

            Property<DateTime>(task => task.Deadline).IsOptional();
            Property<Boolean>(task => task.CanTrackAfterDeadline).IsRequired();
            Property<Boolean>(task => task.IsCompleted).IsRequired();

            HasMany<Work>(task => task.Works)
                .WithRequired(work => work.Trackable)
                .HasForeignKey<Int32>(work => work.TrackableId)
                .WillCascadeOnDelete(false);
        }
    }
}
