﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class PaymentConfiguration : EntityTypeConfiguration<Payment>
    {
        public PaymentConfiguration()
        {
            ToTable("Payments");

            HasKey<Int32>(payment => payment.Id);
            Property<Int32>(payment => payment.Id).IsRequired();

            Property<Int32>(payment => payment.TypeCode).IsRequired();
            Property<Decimal>(payment => payment.Value).IsRequired();

            HasRequired<Currency>(payment => payment.Currency);
        }
    }
}
