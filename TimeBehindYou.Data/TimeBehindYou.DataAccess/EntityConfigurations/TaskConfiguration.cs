﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class TaskConfiguration : EntityTypeConfiguration<Task>
    {
        public TaskConfiguration()
        {
            ToTable("Tasks");

            HasKey<Int32>(task => task.Id);
            Property<Int32>(task => task.Id).IsRequired();

            Property(task => task.Description).IsOptional();
            Property(task => task.Name).IsRequired();

            Property<Boolean>(task => task.IsCompleted).IsRequired();
        }
    }
}
