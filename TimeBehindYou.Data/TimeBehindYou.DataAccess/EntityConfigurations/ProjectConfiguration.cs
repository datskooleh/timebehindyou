﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class ProjectConfiguration : EntityTypeConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            ToTable("Projects");

            HasKey<Int32>(proj => proj.Id);
            Property<Int32>(proj => proj.Id).IsRequired();

            HasMany<Task>(proj => proj.Tasks).WithRequired(task => task.Project)
                .HasForeignKey<Int32>(task => task.ProjectId)
                .WillCascadeOnDelete(true);
        }
    }
}
