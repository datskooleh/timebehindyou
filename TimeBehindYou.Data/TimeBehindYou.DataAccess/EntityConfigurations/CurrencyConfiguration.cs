﻿using System;
using System.Data.Entity.ModelConfiguration;
using TimeBehindYou.Data.Entities;

namespace TimeBehindYou.DataAccess.EntityConfigurations
{
    public class CurrencyConfiguration : EntityTypeConfiguration<Currency>
    {
        public CurrencyConfiguration()
        {
            ToTable("Currencies");

            HasKey<Int32>(curr => curr.Id);
            Property<Int32>(curr => curr.Id).IsRequired();

            Property(curr => curr.Description).IsRequired();
            Property(curr => curr.Abbreviation).IsRequired();
        }
    }
}
