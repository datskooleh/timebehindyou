﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.DataAccess
{
    public class PackageConnection : IConnection, IDisposable
    {
        DbConnection connection;
        public DbConnection GetConnection()
        {
            return connection;
        }

        public PackageConnection(DbConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection = null;
        }
    }
}
