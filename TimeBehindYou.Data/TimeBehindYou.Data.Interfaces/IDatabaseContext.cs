﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Data.Interfaces
{
    public interface IDatabaseContext : IDataQuery, IDisposable
    {
        void Add<TEntity>(TEntity entity) where TEntity : class, IEntity;

        void Add<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;

        void Update<TEntity>(TEntity entity) where TEntity : class, IEntity;

        void Update<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;

        void Delete<TEntity>(TEntity entity) where TEntity : class, IEntity;

        void Delete<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;

        void Detach<TEntity>(TEntity entity) where TEntity : class, IEntity;

        bool SaveChanges();
    }
}
