﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Data.Interfaces
{
    public interface IDataQuery
        
    {
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;

        IQueryable<TEntity> Query<TEntity>(Func<TEntity, bool> func) where TEntity : class, IEntity;

        IQueryable Query<TEntity>(Type type) where TEntity : class, IEntity;
    }
}
