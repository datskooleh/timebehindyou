﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Data.Interfaces
{
    public interface IEntity
    {
        Int32 Id { get; set; }
    }
}
