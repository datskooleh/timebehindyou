﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TimeBehindYou.Data.Entities
{
    public class Project : Trackable
    {
        public Project()
            : base()
        {
            Tasks = new ObservableCollection<Task>();
        }

        //public Int32 UserId { get; set; }
        //public User User{ get; set; }

        public ObservableCollection<Task> Tasks { get; set; }
    }
}
