﻿using System;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Data.Entities
{
    public class Currency : IEntity
    {
        public Int32 Id { get; set; }

        private String description;
        public String Description
        {
            get { return description; }
            set { description = value; }
        }

        public String abbreviation;
        public String Abbreviation
        {
            get { return abbreviation; }
            set { abbreviation = value; }
        }
    }
}
