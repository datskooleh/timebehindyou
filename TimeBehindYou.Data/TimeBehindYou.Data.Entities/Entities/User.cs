﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Claims;
using System.Threading.Tasks;
using TimeBehindYou.Data.Identity;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Data.Entities
{
    public class User : IdentityUser<Int32, UserLogin, UserRole, UserClaim>, IEntity
    {
        public User()
        {
            Projects = new ObservableCollection<Project>();
            WorkingDays = new ObservableCollection<WorkingDay>();
            Payments = new ObservableCollection<Payment>();
        }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public DateTime? Birthday { get; set; }

        public ObservableCollection<Project> Projects { get; set; }

        public ObservableCollection<WorkingDay> WorkingDays { get; set; }

        public ObservableCollection<Payment> Payments { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(TimeBehindYou.Data.Identity.UserManager<User> manager, String authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
