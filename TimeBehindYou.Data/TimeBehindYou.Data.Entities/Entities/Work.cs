﻿using System;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Data.Entities
{
    public class Work : IEntity
    {
        public Int32 Id { get; set; }

        public Int32 WorkingDayId { get; set; }

        public WorkingDay WorkingDay { get; set; }

        public Int32 TrackableId { get; set; }

        public Trackable Trackable { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int Time { get; set; }
    }
}
