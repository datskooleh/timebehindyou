﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Data.Entities
{
    public class WorkingDay : IEntity
    {
        public WorkingDay()
        {
            Works = new ObservableCollection<Work>();
        }

        public Int32 Id { get; set; }

        //public Int32 UserId { get; set; }

        //public User User { get; set; }

        public DateTime Date { get; set; }

        public ObservableCollection<Work> Works { get; set; }
    }
}
