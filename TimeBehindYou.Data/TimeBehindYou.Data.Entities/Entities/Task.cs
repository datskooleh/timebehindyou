﻿using System;

namespace TimeBehindYou.Data.Entities
{
    public class Task : Trackable
    {
        public Task()
            : base()
        {

        }

        public String Description { get; set; }

        public Int32 ProjectId { get; set; }

        public Project Project { get; set; }
    }
}
