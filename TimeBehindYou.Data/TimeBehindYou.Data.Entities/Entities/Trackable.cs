﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Data.Entities
{
    public abstract class Trackable : IEntity
    {
        public Trackable()
            : base()
        {
            Works = new ObservableCollection<Work>();
        }

        public Int32 Id { get; set; }

        public String Name { get; set; }

        public Boolean IsCompleted { get; set; }

        public DateTime? Deadline { get; set; }

        public Boolean CanTrackAfterDeadline { get; set; }

        public ObservableCollection<Work> Works { get; set; }
    }
}
