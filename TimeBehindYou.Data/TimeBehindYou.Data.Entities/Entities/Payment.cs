﻿using TimeBehindYou.Common.Enums;

namespace TimeBehindYou.Data.Entities
{
    using System;
    using TimeBehindYou.Data.Interfaces;

    public class Payment : IEntity
    {
        public Int32 Id { get; set; }

        public Int32 TypeCode { get; set; }
        public PaymentType Type
        {
            get
            {
                return (PaymentType)TypeCode;
            }
            set
            {
                TypeCode = (Int32) value;
            }
        }

        public Decimal Value { get; set; }

        public Int32 CurrencyId { get; set; }
        public Currency Currency { get; set; }
    }
}
