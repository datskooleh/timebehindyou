﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Connection
{
    public class DataConnection : IConnection
    {
        private DbConnection connection;

        public DbConnection GetConnection()
        {
            return connection;
        }

        [InjectionConstructor]
        public DataConnection() : this(Settings.GetConnectionString()) {}
        public DataConnection(string connectionString)
        {
            connection = GetDbConnectionFromConnectionString(connectionString);
        }
        public static DbConnection GetDefaultDbConnection()
        {
            return GetDbConnectionFromConnectionString(Settings.GetConnectionString());
        }
        public static DbConnection GetDbConnectionFromConnectionString(string connectionString)
        {
            DbConnection connection = null;
            string providerName = null;
            var csb = new DbConnectionStringBuilder { ConnectionString = connectionString };

            if (csb.ContainsKey("provider"))
            {
                providerName = csb["provider"].ToString();
            }
            else
            {
                var css = ConfigurationManager
                                  .ConnectionStrings
                                  .Cast<ConnectionStringSettings>()
                                  .FirstOrDefault(x => x.ConnectionString == connectionString);
                if (css != null) providerName = css.ProviderName;
            }

            if (String.IsNullOrEmpty(providerName))
            {
                providerName = "System.Data.SqlClient";
            }
            if (providerName != null)
            {
                var providerExists = DbProviderFactories
                                            .GetFactoryClasses()
                                            .Rows.Cast<DataRow>()
                                            .Any(r => r[2].Equals(providerName));
                if (providerExists)
                {
                    var factory = DbProviderFactories.GetFactory(providerName);
                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;
                }
            }

            return connection;
        }
        public void Dispose()
        {
            connection.Dispose();
            connection = null;
        }
    }
}
