﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Connection
{
    public class Settings
    {
        private static string connectionstring = "";

        static Settings()
        {
            if (String.IsNullOrEmpty(connectionstring))
            {
                var directory = System.Environment.CurrentDirectory + @"\config.ini";
                Console.WriteLine("using " + directory);
                if (!File.Exists(directory))
                {
                    throw new Exception(directory + " not found");
                }

                try
                {
                    var parser = new FileIniDataParser();

                    IniData data = parser.ReadFile("config.ini");
                    connectionstring = data["Data"]["Connection"];

                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
        public static string GetConnectionString()
        {
            return connectionstring;
        }
    }
}
