﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Data.Interfaces;

namespace TimeBehindYou.Connection
{
    public class Bootstrapper
    {
        public static void RegisterWithContainer(IUnityContainer container)
        {
            container.RegisterType<IConnection, DataConnection>();
        }
    }
}
