﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Common.Enums
{
    public enum ProcessingResult
    {
        Ok,
        CantProcess,
        BadInput,
        NoContent
    }
}
