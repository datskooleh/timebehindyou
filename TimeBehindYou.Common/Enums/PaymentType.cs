﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeBehindYou.Common.Enums
{
    public enum PaymentType
    {
        [Display(Name = "Hourly")]
        Hourly = 1,
        [Display(Name = "Monthly")]
        Monthly = 2,
        [Display(Name = "Fixed")]
        Fixed = 4,
        [Display(Name = "Once")]
        Once = 8
    }
}
