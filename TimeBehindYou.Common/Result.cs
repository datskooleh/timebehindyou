﻿using TimeBehindYou.Common.Enums;

namespace TimeBehindYou.Common
{
    public class Result<TType>
    {
        public TType Data { get; set; }

        public ProcessingResult Status { get; set; }
    }
}
