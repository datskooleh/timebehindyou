﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TimeBehindYou.Services.Common;

namespace TimeBehindYou.API.Configuration
{
    public class ServicesConfiguration
    {
        public IUnityContainer Container { get; private set; }

        private readonly List<Type> _unavailableModules = new List<Type>();

        public ServicesConfiguration()
        {
            try
            {
                Container = new UnityContainer();

                SetupUnity();

                var bootstrappers =
                    new ReadOnlyCollection<IServiceBootstrapper>(
                        Assembly.GetExecutingAssembly().GetReferencedAssemblies()
                            .Where(x => x.FullName.StartsWith("TimeBehindYou.Services."))
                            .Select(Assembly.Load)
                            .SelectMany(x => x.GetTypes())
                            .Where(x => x.GetInterfaces().Contains(typeof(IServiceBootstrapper)))
                            .Select(Activator.CreateInstance)
                            .Cast<IServiceBootstrapper>()
                            .ToList());
                
                foreach (var bootstrapper in bootstrappers)
                {
                    try
                    {
                        Console.WriteLine("Configuring Unity for {0}", bootstrapper.GetType().Module.Name);
                        bootstrapper.ConfigureUnity(Container);
                        Console.WriteLine("Configuring AutoMapper for {0}", bootstrapper.GetType().Module.Name);
                        bootstrapper.ConfigureMappings();
                    }
                    catch (Exception ex)
                    {
                        _unavailableModules.AddRange(bootstrapper.GetType().Assembly.GetTypes().Where(x => x.GetInterfaces().Contains(typeof(IService))));
                    }
                }
                if (_unavailableModules.Any())
                {
                    Console.WriteLine("The following modules could not be configured and are unavailable:");
                    foreach (var module in _unavailableModules.Select(x => x.Module.Name).Distinct())
                        Console.WriteLine("\t- {0}", module);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetupUnity()
        {
            Container.RegisterInstance("SystemContainer");
            DataAccess.Bootstrapper.RegisterWithContainer(Container);
            
            //Data.Entities.Bootstrapper.RegisterWithContainer(Container);
        }

        public TModule GetModule<TModule>()
            where TModule : class, IService
        {
            if(_unavailableModules.Contains(typeof(TModule)))
                throw new Exception(String.Format("{0} is unavailable, please check its bootstrapper for errors.", typeof(TModule).Module.Name));
            return Container.Resolve<TModule>();
        }
    }
}
