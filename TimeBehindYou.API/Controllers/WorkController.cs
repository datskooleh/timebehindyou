﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeBehindYou.API.Base;

namespace TimeBehindYou.API.Controllers
{
    [RoutePrefix("api")]
    public class WorkController : BaseApiController
    {
        [HttpGet]
        [Route("{userId:int}/works/{count=0:int}")]
        public IHttpActionResult Get([FromUri] Int32 userId, [FromUri] Int32 count = 0)
        {
            //var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.User.Service>();

            return Ok();
        }

        [HttpPost]
        [Route("{userId:int}/works")]
        public IHttpActionResult Post()
        {
            //var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.User.Service>();

            return Ok();
        }

        [HttpPut]
        [Route("{userId:int}/work")]
        public IHttpActionResult Put()
        {
            return Ok();
        }

        [HttpDelete]
        [Route("{userId:int}/works/{entryId:int}")]
        public IHttpActionResult Delete([FromUri] Int32 userId, [FromUri] Int32 workId)
        {
            return Ok();
        }
    }
}
