﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using TimeBehindYou.API.Base;
using TimeBehindYou.Services.User.Models;

namespace TimeBehindYou.API.Controllers
{
    [RoutePrefix("api")]
    public class UserController : BaseApiController
    {
        [HttpGet]
        [Route("{userId:int}/accounts/{count=0:int}")]
        public IHttpActionResult Get([FromUri] Int32 userId, [FromUri] Int32 count = 0)
        {
            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.User.Service>();

            var user = new User()
                {
                    Id = 0,
                    FirstName = "Oleh",
                    LastName = "Datsko"
                };

            var result = module.Save(user);

            return Ok(result);
        }

        [HttpPost]
        [Route("{userId:int}/accounts")]
        public IHttpActionResult Post([FromUri] Int32 userId, [FromBody] User user)
        {
            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.User.Service>();

            user = new User()
            {
                Id = 0,
                FirstName = "Oleh",
                LastName = "Datsko"
            };

            var result = module.Save(user);

            return Ok(result);
        }

        [HttpPut]
        [Route("{userId:int}/accounts")]
        public IHttpActionResult Put([FromUri] Int32 userId, [FromBody] User user)
        {
            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.User.Service>();

            user = new User()
            {
                Id = 0,
                FirstName = "Oleh",
                LastName = "Datsko"
            };

            var result = module.Save(user);

            return Ok(result);
        }

        [HttpDelete]
        [Route("{userId:int}/accounts/{accountId:int}")]
        public IHttpActionResult Delete([FromUri] Int32 userId, [FromUri] Int32 accountId)
        {
            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.User.Service>();

            var user = new User()
            {
                Id = 0,
                FirstName = "Oleh",
                LastName = "Datsko"
            };

            var result = module.Save(user);

            return Ok(result);
        }
    }
}
