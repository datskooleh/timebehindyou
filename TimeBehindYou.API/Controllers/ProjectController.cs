﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TimeBehindYou.API.Base;
using TimeBehindYou.Common;
using TimeBehindYou.Services.Project.Models;

namespace TimeBehindYou.API.Controllers
{
    [RoutePrefix("api")]
    public class ProjectController : BaseApiController
    {
        [HttpGet]
        [Route("{userId:int}/projects/{count=0:int}")]
        public IEnumerable<Project> Get([FromUri] Int32 userId, [FromUri] Int32 count = 0)
        {
            return new List<Project>() { new Project() { Id = 5 }, new Project() { Id = 100500 } };
        }

        [HttpPost]
        [Route("projects")]
        public IHttpActionResult Post([FromBody] Project project)
        {
            Result<Project> result = null;

            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.Project.Service>();

            if (project.Tasks == null)
                project.Tasks = new List<Task>();

            result = module.Add(project);

            return CheckResult<Project>(result);
        }

        [HttpPut]
        [Route("projects")]
        public IHttpActionResult Put([FromBody] Project project)
        {
            Result<Project> result = null;
            if(project == null)
            {
                result = new Result<Project>();
                result.Status = Common.Enums.ProcessingResult.BadInput;

                return CheckResult<Project>(result);
            }

            if (project.Tasks == null)
                project.Tasks = new List<Task>();


            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.Project.Service>();
            result = module.Update(project);

            return CheckResult<Project>(result);
        }

        [HttpDelete]
        [Route("{userId:int}/projects/{projectId:int}")]
        public IHttpActionResult Delete([FromUri] Int32 userId, [FromUri] Int32 projectId)
        {
            Result<Project> result = null;

            var module = BaseApiController.Config.GetModule<TimeBehindYou.Services.Project.Service>();
            //module.Remove(projectId);

            return CheckResult<Project>(result);
        }
    }
}
