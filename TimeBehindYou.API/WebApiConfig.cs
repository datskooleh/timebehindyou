﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TimeBehindYou.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{userId:int}/{controller}",
                defaults: new { id = -1 }
            );

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.EnsureInitialized();
        }
    }
}
