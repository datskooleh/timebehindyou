﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using TimeBehindYou.Auth;

[assembly: OwinStartup(typeof(TimeBehindYou.API.Startup))]

namespace TimeBehindYou.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888

            var config = new HttpConfiguration();

            WebApiConfig.Register(config);
            //app.ConfigureAuth();

            app.UseWebApi(config);
        }
    }
}
