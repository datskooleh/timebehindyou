﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using TimeBehindYou.API.Configuration;
using TimeBehindYou.Common;

namespace TimeBehindYou.API.Base
{
    public class BaseApiController : ApiController
    {
        public static readonly ServicesConfiguration Config = new ServicesConfiguration();

        protected IHttpActionResult CheckResult<TModel>(Result<TModel> result)
        {
            if (result.Status == Common.Enums.ProcessingResult.Ok)
                return Ok(result.Data);
            else if (result.Status == Common.Enums.ProcessingResult.NoContent)
                return NotFound();
            else if (result.Status == Common.Enums.ProcessingResult.BadInput)
                return BadRequest("Not valid data");

            return InternalServerError();
        }
    }
}
